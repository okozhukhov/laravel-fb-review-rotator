﻿
<!-- / PARTIAL Only relevant code left \ -->

<section class="reviewBox text-center">
	<div class="center">
		<!--<h2>Wat onze klanten zeggen</h2>-->
		<div class="side">
		<a href="https://www.facebook.com/__FB_PAGE__/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
		<a href="https://twitter.com/__FB_PAGE__" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a>
		<a href="#" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
		<span><?php echo $pagelikes; ?> Likes</span>
		<div style="clear:both;margin-bottom:20px;"></div>
		<a class="albutton" href="https://www.facebook.com/__FB_PAGE__/" target="_blank">Vind ons leuk op Facebook</a>
		<a class="albutton offcolor" href="https://www.facebook.com/pg/__FB_PAGE__/reviews/" target="_blank">Meer beoordelingen lezen</a>
		<a class="albutton secolor" href="./offerte">Vraag offerte aan</a>
		</div>
		<div class="quote">
		
			<img src='./templates/frontend/images/quote.png' alt=''>
			<ul class="cycle-slideshow" data-cycle-fx="fade" data-cycle-speed=1000 data-cycle-timeout=8000 data-cycle-swipe="true" data-cycle-pager=".cycle-pager" data-cycle-slides=">
						li,> img,> div.slide" >
						
				<?php
				$c = 0;
				$REVS = "";
				$flag = "";
					foreach($reviews as $item) if($c < 3){
						if(isset($item['review_text'])){
							$c++;
						$star = "";
						switch($item['rating']){
							case '1':
								$star = "<i class=\"fa fa-star\"></i>";
							break;
							case '2':
								$star = "<i class=\"fa fa-star\"></i> <i class=\"fa fa-star\"></i>";
							break;
							case '3':
								$star = "<i class=\"fa fa-star\"></i> <i class=\"fa fa-star\"> </i><i class=\"fa fa-star\"></i>";
							break;
							case '4':
								$star = "<i class=\"fa fa-star\"></i> <i class=\"fa fa-star\"></i> <i class=\"fa fa-star\"></i> <i class=\"fa fa-star\"></i>";
							break;
							case '5':
								$star = "<i class=\"fa fa-star\"></i> <i class=\"fa fa-star\"></i> <i class=\"fa fa-star\"></i> <i class=\"fa fa-star\"></i> <i class=\"fa fa-star\"></i>";
							break;
						}
						$REVS .= "
							<li>
							<h4>" . $item['reviewer']['name'] . "</h4><br>
							<div class='text'>
								<p>" . $item['review_text'] . "</p>
							</div>
							<small>" . $star . "</small>
							</li>
						";
						}
					}
				
				echo $REVS;
				?>
			</ul>
			<div class="cycle-pager"></div>
		</div>
		<div style="clear:both"></div>
	</div>
</section>