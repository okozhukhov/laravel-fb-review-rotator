<?php
//PARTIAL File only relevant code left

public function index()
	{
        $seo    = Seotags::first();
		
		$code = "__FB__Permanent_access_Code__";
		$json_url = "https://graph.facebook.com/v2.11/[__FB__ID__]/?fields=fan_count,ratings&access_token=" . $code;

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL,$json_url);
		$result=curl_exec($ch);
		curl_close($ch);

		$data = json_decode($result, true);
		$reviews = $data['ratings']['data'];
		
		$pagelikes = $data['fan_count'];
		
        return view('layouts.general', compact('page', 'seo', 'reviews', 'pagelikes'));
	}

